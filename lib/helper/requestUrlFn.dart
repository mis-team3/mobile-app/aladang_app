import 'dart:convert';
import 'package:http/http.dart' as http;

fnGet({
  required String url,
  String? param1,
  String? param2,
  String? param3,
  String? param4,
  String? param5,
  String? param6,
  String? param7,
  String? param8,
  String? param9,
  String? param10,
  String? param11,
  String? param12,
  String? param13,
  String? param14,
  String? param15,
  String? param16,
  String? param17,
  String? param18,
  String? param19,
  String? param20,
  String? param21,
  String? param22,
  String? param23,
  String? param24,
  String? param25,
  String? param26,
  String? val1,
  String? val2,
  String? val3,
  String? val4,
  String? val5,
  String? val6,
  String? val7,
  String? val8,
  String? val9,
  String? val10,
  String? val11,
  String? val12,
  String? val13,
  String? val14,
  String? val15,
  String? val16,
  String? val17,
  String? val18,
  String? val19,
  String? val20,
  String? val21,
  String? val22,
  String? val23,
  String? val24,
  String? val25,
  String? val26,
}) async {
  var resp;
  if (param1 == null) {
    resp = await http.get(Uri.parse(url));
  }

  if (param1 != null && param2 == null) {
    resp = await http.post(Uri.parse(url), body: {param1: val1});
  }

  if (param2 != null && param3 == null) {
    resp = await http.post(Uri.parse(url), body: {param1: val1, param2: val2});
  }

  if (param3 != null && param4 == null) {
    resp = await http
        .post(Uri.parse(url), body: {param1: val1, param2: val2, param3: val3});
  }

  if (param4 != null && param5 == null) {
    resp = await http.post(Uri.parse(url),
        body: {param1: val1, param2: val2, param3: val3, param4: val4});
  }

  if (param5 != null && param6 == null) {
    resp = await http.post(Uri.parse(url), body: {
      param1: val1,
      param2: val2,
      param3: val3,
      param4: val4,
      param5: val5
    });
  }

  if (param6 != null && param7 == null) {
    resp = await http.post(Uri.parse(url), body: {
      param1: val1,
      param2: val2,
      param3: val3,
      param4: val4,
      param5: val5,
      param6: val6
    });
  }

  if (param7 != null && param8 == null) {
    resp = await http.post(Uri.parse(url), body: {
      param1: val1,
      param2: val2,
      param3: val3,
      param4: val4,
      param5: val5,
      param6: val6,
      param7: val7
    });
  }

  if (param8 != null && param9 == null) {
    resp = await http.post(Uri.parse(url), body: {
      param1: val1,
      param2: val2,
      param3: val3,
      param4: val4,
      param5: val5,
      param6: val6,
      param7: val7,
      param8: val8
    });
  }

  if (param9 != null && param10 == null) {
    resp = await http.post(Uri.parse(url), body: {
      param1: val1,
      param2: val2,
      param3: val3,
      param4: val4,
      param5: val5,
      param6: val6,
      param7: val7,
      param8: val8,
      param9: val9
    });
  }

  if (param10 != null && param11 == null) {
    resp = await http.post(Uri.parse(url), body: {
      param1: val1,
      param2: val2,
      param3: val3,
      param4: val4,
      param5: val5,
      param6: val6,
      param7: val7,
      param8: val8,
      param9: val9,
      param10: val10
    });
  }

  if (param11 != null && param12 == null) {
    resp = await http.post(Uri.parse(url), body: {
      param1: val1,
      param2: val2,
      param3: val3,
      param4: val4,
      param5: val5,
      param6: val6,
      param7: val7,
      param8: val8,
      param9: val9,
      param10: val10,
      param11: val11,
    });
  }

  if (param12 != null && param13 == null) {
    resp = await http.post(Uri.parse(url), body: {
      param1: val1,
      param2: val2,
      param3: val3,
      param4: val4,
      param5: val5,
      param6: val6,
      param7: val7,
      param8: val8,
      param9: val9,
      param10: val10,
      param11: val11,
      param12: val12
    });
  }

  if (param13 != null) {
    resp = await http.post(Uri.parse(url), body: {
      param1: val1,
      param2: val2,
      param3: val3,
      param4: val4,
      param5: val5,
      param6: val6,
      param7: val7,
      param8: val8,
      param9: val9,
      param10: val10,
      param11: val11,
      param12: val12,
      param13: val13,
    });
  }
  if (param14 != null) {
    resp = await http.post(Uri.parse(url), body: {
      param1: val1,
      param2: val2,
      param3: val3,
      param4: val4,
      param5: val5,
      param6: val6,
      param7: val7,
      param8: val8,
      param9: val9,
      param10: val10,
      param11: val11,
      param12: val12,
      param13: val13,
    });
  }
  if (param13 != null) {
    resp = await http.post(Uri.parse(url), body: {
      param1: val1,
      param2: val2,
      param3: val3,
      param4: val4,
      param5: val5,
      param6: val6,
      param7: val7,
      param8: val8,
      param9: val9,
      param10: val10,
      param11: val11,
      param12: val12,
      param13: val13,
    });
  }
  if (param13 != null) {
    resp = await http.post(Uri.parse(url), body: {
      param1: val1,
      param2: val2,
      param3: val3,
      param4: val4,
      param5: val5,
      param6: val6,
      param7: val7,
      param8: val8,
      param9: val9,
      param10: val10,
      param11: val11,
      param12: val12,
      param13: val13,
    });
  }
  if (param13 != null) {
    resp = await http.post(Uri.parse(url), body: {
      param1: val1,
      param2: val2,
      param3: val3,
      param4: val4,
      param5: val5,
      param6: val6,
      param7: val7,
      param8: val8,
      param9: val9,
      param10: val10,
      param11: val11,
      param12: val12,
      param13: val13,
    });
  }
  if (param13 != null) {
    resp = await http.post(Uri.parse(url), body: {
      param1: val1,
      param2: val2,
      param3: val3,
      param4: val4,
      param5: val5,
      param6: val6,
      param7: val7,
      param8: val8,
      param9: val9,
      param10: val10,
      param11: val11,
      param12: val12,
      param13: val13,
    });
  }
  if (param13 != null) {
    resp = await http.post(Uri.parse(url), body: {
      param1: val1,
      param2: val2,
      param3: val3,
      param4: val4,
      param5: val5,
      param6: val6,
      param7: val7,
      param8: val8,
      param9: val9,
      param10: val10,
      param11: val11,
      param12: val12,
      param13: val13,
    });
  }
  if (param13 != null) {
    resp = await http.post(Uri.parse(url), body: {
      param1: val1,
      param2: val2,
      param3: val3,
      param4: val4,
      param5: val5,
      param6: val6,
      param7: val7,
      param8: val8,
      param9: val9,
      param10: val10,
      param11: val11,
      param12: val12,
      param13: val13,
    });
  }
  if (param13 != null) {
    resp = await http.post(Uri.parse(url), body: {
      param1: val1,
      param2: val2,
      param3: val3,
      param4: val4,
      param5: val5,
      param6: val6,
      param7: val7,
      param8: val8,
      param9: val9,
      param10: val10,
      param11: val11,
      param12: val12,
      param13: val13,
    });
  }
  if (param13 != null) {
    resp = await http.post(Uri.parse(url), body: {
      param1: val1,
      param2: val2,
      param3: val3,
      param4: val4,
      param5: val5,
      param6: val6,
      param7: val7,
      param8: val8,
      param9: val9,
      param10: val10,
      param11: val11,
      param12: val12,
      param13: val13,
    });
  }
  if (param13 != null) {
    resp = await http.post(Uri.parse(url), body: {
      param1: val1,
      param2: val2,
      param3: val3,
      param4: val4,
      param5: val5,
      param6: val6,
      param7: val7,
      param8: val8,
      param9: val9,
      param10: val10,
      param11: val11,
      param12: val12,
      param13: val13,
    });
  }
  if (param13 != null) {
    resp = await http.post(Uri.parse(url), body: {
      param1: val1,
      param2: val2,
      param3: val3,
      param4: val4,
      param5: val5,
      param6: val6,
      param7: val7,
      param8: val8,
      param9: val9,
      param10: val10,
      param11: val11,
      param12: val12,
      param13: val13,
    });
  }
  if (param13 != null) {
    resp = await http.post(Uri.parse(url), body: {
      param1: val1,
      param2: val2,
      param3: val3,
      param4: val4,
      param5: val5,
      param6: val6,
      param7: val7,
      param8: val8,
      param9: val9,
      param10: val10,
      param11: val11,
      param12: val12,
      param13: val13,
    });
  }
  if (param13 != null) {
    resp = await http.post(Uri.parse(url), body: {
      param1: val1,
      param2: val2,
      param3: val3,
      param4: val4,
      param5: val5,
      param6: val6,
      param7: val7,
      param8: val8,
      param9: val9,
      param10: val10,
      param11: val11,
      param12: val12,
      param13: val13,
    });
  }
  if (resp.statusCode == 200) {
    try {
      var data = jsonDecode(resp.body);
      return data;
    } catch (e) {
      return '==============> Something went wrong, Please check again. <=============={$e}';
    }
  } else {
    return '==============> return StatusCode not 200, Please check again. <==============';
  }
}
