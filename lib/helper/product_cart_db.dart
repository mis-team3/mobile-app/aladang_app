import 'package:sqflite/sqflite.dart' as sql;
import 'package:sqflite/sqflite.dart';

class ProductCartDB {
  // ignore: constant_identifier_names
  static const product_cart_tbl = """
  CREATE TABLE product_cart(
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 
        productId INTEGER,
        productCode TEXT,
        productName TEXT, 
        price DOUBLE NULL,
        qty integer
      )""";

  static Future<void> createTables(sql.Database database) async {
    await database.execute(product_cart_tbl);
  }

  static Future<sql.Database> db() async {
    return sql.openDatabase(
      'AladangDb.db',
      version: 1,
      onCreate: (sql.Database database, int version) async {
        await createTables(database);
      },
    );
  }

  // Create new item
  static Future<int> insertProductCart(
      int proId, String proCode, String proName, double price) async {
    final db = await ProductCartDB.db();

    final data = {
      'productId': proId,
      'productCode': proCode,
      'productName': proName,
      'price': price,
      'qty': 1
    };
    final id = await db.insert('product_cart', data,
        conflictAlgorithm: sql.ConflictAlgorithm.replace);
    print(id);
    return id;
  }

  // Read all items
  static Future<List<Map<String, dynamic>>> getProductCarts() async {
    final db = await ProductCartDB.db();
    return db.query('product_cart', orderBy: "id");
  }

  static Future<List<Map<String, dynamic>>> getProductCartCount() async {
    final db = await ProductCartDB.db();
    return db.rawQuery(
        'select productId,productCode,productName,price,qty from product_cart');
  }

  static Future<void> truncateProductCart() async {
    final db = await ProductCartDB.db();
    db.rawQuery('delete from product_cart');
  }

  static Future<void> delete(int id) async {
    final db = await ProductCartDB.db();
    await db.rawQuery('update product_cart set qty=qty-1  where productId=$id');
  }

  static Future getSumTotal() async {
    final db = await ProductCartDB.db();
    return await db.rawQuery(
        'select sum(price) as total from product_cart group by price');
  }

  static Future coutItem() async {
    final db = await ProductCartDB.db();
    return await db.rawQuery('select count(*) as count from product_cart');
  }

  static Future updateOld(pid) async {
    final db = await ProductCartDB.db();
    return await db
        .rawQuery('update product_cart set qty=qty+1  where productId=$pid');
  }

  static Future insertNew(pid, pcode, pname, price, qty) async {
    final db = await ProductCartDB.db();
    return await db.rawQuery(
        'insert into product_cart values($pid,$pcode,$pname,$price,$qty)');
  }

  static Future<dynamic> insert(pid, pcode, pname, price, qty) async {
    final db = await ProductCartDB.db();
    int? count = Sqflite.firstIntValue(await db
        .rawQuery("SELECT COUNT(*) FROM product_cart where productId=$pid"));
    if (count == 0) {
      insertProductCart(
        pid,
        pcode,
        pname,
        price,
      );
      // insertNew(pid, pcode, pname, price, qty);
    } else {
      updateOld(pid);
    }
  }

  static Future<int> getcount() async {
    final db = await ProductCartDB.db();
    int? count = Sqflite.firstIntValue(
        await db.rawQuery("SELECT COUNT(*) FROM product_cart"));
    print(count);
    return count!;
  }

  static Future insertNew1(pid, pcode, pname, price, qty) async {
    final db = await ProductCartDB.db();
    return await db.rawQuery(
        'select  insert into product_cart values($pid,$pcode,$pname,$price,$qty)');
  }
}
