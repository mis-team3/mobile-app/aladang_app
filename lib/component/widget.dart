import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

Widget CustomText(String txt, int getNumLetter) {
  // return txt != null
  //     ? Text(
  //         txt.length == 15 ? '${txt.substring(0, 15)}...' : txt.toString(),
  //       )
  //     : const Text(
  //         "",
  //       );
  return Text(txt);
}

imageExists(String url) async {
  try {
    final response = await http.get(Uri.parse(url));
    return response.statusCode == 200
        ? Image.network(
            url,
            height: 100,
            width: 110,
            errorBuilder: (context, error, stackTrace) {
              return Container(
                height: 100,
                decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: BorderRadius.circular(10),
                ),
                child: const Center(
                  child: Icon(
                    Icons.image_not_supported_outlined,
                    size: 50,
                    color: Colors.black54,
                  ),
                ),
              );
            },
          )
        : Container(
            height: 100,
            decoration: BoxDecoration(
              color: Colors.grey[300],
              borderRadius: BorderRadius.circular(10),
            ),
            child: const Center(
              child: Icon(
                Icons.image_not_supported_outlined,
                size: 50,
                color: Colors.black54,
              ),
            ),
          );
  } catch (e) {
    return const SizedBox();
  }
}
