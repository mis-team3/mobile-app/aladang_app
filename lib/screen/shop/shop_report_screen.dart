import 'package:aladang_app/component/input_datetime_widget.dart';
import 'package:aladang_app/helpdata/report_data.dart';
import 'package:aladang_app/helpdata/repuest_function.dart';
import 'package:aladang_app/model/order/Order.dart';
import 'package:aladang_app/model/report/ReportOrderDetail.dart';
import 'package:aladang_app/servies_provider/provider_url.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../helpdata/product_data.dart';
import '../../model/customer/Customer.dart';
import '../../model/product/Product.dart';
import '../../model/report/ReportOrderDetailRes.dart';
import '../../utils/constant.dart';

class ShopReport extends StatefulWidget {
  const ShopReport({Key? key}) : super(key: key);

  @override
  State<ShopReport> createState() => _ShopReportState();
}

class _ShopReportState extends State<ShopReport> {
  TextEditingController txtFromDate = TextEditingController();
  TextEditingController txtToDate = TextEditingController();

  List<Customer>? customerList = [];
  List<Product>? productList = [];
  List<Order>? orderList = [];
  List<ReportOrderDetailRes>? reportOrderDetailList = [];

  List dataList = [];

  void getCustomerList(id) async {
    final result = await ReportData().getReportStockByShop(id);
    setState(() {
      productList = result.data!;
    });
  }

  void getProductList(id) async {
    final result = await ReportData().getReportStockByShop(id);
    setState(() {
      productList = result.data!;
    });
  }

  void getOrderList(id) async {
    final result = await ReportData().getReportOrderByShop(id);
    setState(() {
      orderList = result.data!;
    });
  }

  void getReportOrderDetial(id) async {
    var result = await ReportData().getReportOorderDetailByShopId(id);
    setState(() {
      reportOrderDetailList = result.data!;
    });
  }

  void getLogint() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      getCustomerList(prefs.getInt(SHOP_ID));
      getProductList(prefs.getInt(SHOP_ID));
      getOrderList(prefs.getInt(SHOP_ID));
      getReportOrderDetial(prefs.getInt(SHOP_ID));
    });
  }

  @override
  void initState() {
    getLogint();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          bottom: TabBar(
            // labelColor: Colors.white,
            // unselectedLabelColor: Colors.grey,
            dividerColor: Colors.transparent,

            indicatorSize: TabBarIndicatorSize.tab,
            // isScrollable: true,
            unselectedLabelColor: Colors.white.withOpacity(0.8),
            indicatorColor: Colors.green,
            labelColor: Colors.white,
            onTap: (selectedIndex) {},
            tabs: [
              Tab(
                // text: "All ${productAll.length}",
                child: Text(
                  "stock".tr(),
                  style: const TextStyle(fontSize: textSize),
                ),
              ),
              Tab(
                // text: "Active $countActive",
                child: Text(
                  "sale".tr(),
                  style: const TextStyle(fontSize: textSize),
                ),
              ),
              Tab(
                // text: "Active $countActive",
                child: Text(
                  "customer".tr(),
                  style: const TextStyle(fontSize: textSize),
                ),
              ),
            ],
          ),
          title: Text(
            "report".tr(),
            style: const TextStyle(fontSize: textSizeTitle),
          ),
          backgroundColor: primary,
        ),
        body: TabBarView(
          children: [
            _stockReport(context),
            _saleReport(context),
            _customerReport(context),
          ],
        ),
      ),
    );
  }

  Widget _stockReport(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: Offset(0, 2),
                      ),
                    ],
                  ),
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: DataTable(
                      //horizontalMargin: 100,
                      //columnSpacing: 50,
                      columns: const <DataColumn>[
                        DataColumn(
                          label: Expanded(
                            child: Text(
                              'ID',
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        DataColumn(
                          label: Expanded(
                            child: Text(
                              'Code',
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        DataColumn(
                          label: Expanded(
                            child: Text(
                              'Name',
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        DataColumn(
                          label: Expanded(
                            child: Text(
                              'Qty',
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        DataColumn(
                          label: Expanded(
                            child: Text(
                              'Price',
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        DataColumn(
                          label: Expanded(
                            child: Text(
                              'Currency',
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        DataColumn(
                          label: Expanded(
                            child: Text(
                              'Stock Type',
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        DataColumn(
                          label: Expanded(
                            child: Text(
                              'Qty alert',
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        DataColumn(
                          label: Expanded(
                            child: Text(
                              'Expire date',
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ],

                      rows: List<DataRow>.generate(
                        productList!.length,
                        (index) => DataRow(
                          cells: [
                            DataCell(
                              Text(
                                "${productList![index].id}",
                                style: const TextStyle(
                                  fontSize: 10,
                                ),
                              ),
                            ),
                            DataCell(
                              Text(
                                "${productList![index].productCode}",
                                style: const TextStyle(
                                  fontSize: 10,
                                ),
                              ),
                            ),
                            DataCell(
                              Text(
                                "${productList![index].productName}",
                                style: const TextStyle(
                                  fontSize: 10,
                                ),
                              ),
                            ),
                            DataCell(
                              Text(
                                "${productList![index].qtyInStock}",
                                style: const TextStyle(
                                  fontSize: 10,
                                ),
                              ),
                            ),
                            DataCell(
                              Text(
                                "${productList![index].price}",
                                style: const TextStyle(
                                  fontSize: 10,
                                ),
                              ),
                            ),
                            DataCell(
                              Text(
                                "${productList![index].currencyId}",
                                style: const TextStyle(
                                  fontSize: 10,
                                ),
                              ),
                            ),
                            DataCell(
                              Text(
                                "${productList![index].cutStockType}",
                                style: const TextStyle(
                                  fontSize: 10,
                                ),
                              ),
                            ),
                            DataCell(
                              Text(
                                "${productList![index].qtyInStock}",
                                style: const TextStyle(
                                  fontSize: 10,
                                ),
                              ),
                            ),
                            DataCell(
                              Text(
                                DateFormat('dd-MMM-yyyy').format(DateTime.parse(
                                    productList![index].expiredDate!)),
                                style: const TextStyle(
                                  fontSize: 10,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget _saleReport(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: Offset(0, 2),
                      ),
                    ],
                  ),
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: DataTable(
                      //horizontalMargin: 100,
                      //columnSpacing: 50,
                      columns: const <DataColumn>[
                        DataColumn(
                          label: Expanded(
                            child: Text(
                              'ID',
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        DataColumn(
                          label: Expanded(
                            child: Text(
                              'Invoice No',
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        DataColumn(
                          label: Expanded(
                            child: Text(
                              'Date',
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        DataColumn(
                          label: Expanded(
                            child: Text(
                              'Phone',
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        DataColumn(
                          label: Expanded(
                            child: Text(
                              'Customer',
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        DataColumn(
                          label: Expanded(
                            child: Text(
                              'Delivery',
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ],

                      rows: List<DataRow>.generate(
                        reportOrderDetailList!.length,
                        (index) => DataRow(
                          cells: [
                            DataCell(
                              Text(
                                "${reportOrderDetailList![index].orderid}",
                                style: const TextStyle(
                                  fontSize: 10,
                                ),
                              ),
                            ),
                            DataCell(
                              Text(
                                "${reportOrderDetailList![index].invoiceNo}",
                                style: const TextStyle(
                                  fontSize: 10,
                                ),
                              ),
                            ),
                            DataCell(
                              Text(
                                DateFormat('yyyy-MM-dd').format(DateTime.parse(
                                    reportOrderDetailList![index].date!)),
                                style: const TextStyle(
                                  fontSize: 10,
                                ),
                              ),
                            ),
                            DataCell(
                              Text(
                                "${reportOrderDetailList![index].phone}",
                                style: const TextStyle(
                                  fontSize: 10,
                                ),
                              ),
                            ),
                            DataCell(
                              Text(
                                "${reportOrderDetailList![index].customerId == 0 ? reportOrderDetailList![index].shopId : reportOrderDetailList![index].customerId}",
                                style: const TextStyle(
                                  fontSize: 10,
                                ),
                              ),
                            ),
                            DataCell(
                              Text(
                                "${reportOrderDetailList![index].deliveryTypeIn}",
                                style: const TextStyle(
                                  fontSize: 10,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget _customerReport(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: Offset(0, 2),
                      ),
                    ],
                  ),
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: DataTable(
                      //horizontalMargin: 100,
                      //columnSpacing: 50,
                      columns: <DataColumn>[
                        DataColumn(
                          label: Expanded(
                            child: Text(
                              '#',
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        DataColumn(
                          label: Expanded(
                            child: Text(
                              'customer_id'.tr(),
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        DataColumn(
                          label: Expanded(
                            child: Text(
                              'customer_name'.tr(),
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        DataColumn(
                          label: Expanded(
                            child: Text(
                              'gender'.tr(),
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        DataColumn(
                          label: Expanded(
                            child: Text(
                              'phone'.tr(),
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        DataColumn(
                          label: Expanded(
                            child: Text(
                              'address'.tr(),
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        DataColumn(
                          label: Expanded(
                            child: Text(
                              'register_date'.tr(),
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ],

                      rows: List<DataRow>.generate(
                        dataList.length,
                        (index) => DataRow(
                          cells: [
                            DataCell(
                              Text(
                                "${index + 1}",
                                style: const TextStyle(
                                  fontSize: 10,
                                ),
                              ),
                            ),
                            DataCell(
                              Text(
                                "${dataList[index]['customerId']}",
                                style: const TextStyle(
                                  fontSize: 10,
                                ),
                              ),
                            ),
                            DataCell(
                              Text(
                                "${dataList[index]['customerName']}",
                                style: const TextStyle(
                                  fontSize: 10,
                                ),
                              ),
                            ),
                            DataCell(
                              Text(
                                "${dataList[index]['gender']}",
                                style: const TextStyle(
                                  fontSize: 10,
                                ),
                              ),
                            ),
                            DataCell(
                              Text(
                                "${dataList[index]['phone']}",
                                style: const TextStyle(
                                  fontSize: 10,
                                ),
                              ),
                            ),
                            DataCell(
                              Text(
                                "${dataList[index]['currentLocation']}",
                                style: const TextStyle(
                                  fontSize: 10,
                                ),
                              ),
                            ),
                            DataCell(
                              Text(
                                DateFormat('dd-MMM-yyyy').format(
                                  DateTime.parse(
                                    dataList[index]['date'],
                                  ),
                                ),
                                style: const TextStyle(
                                  fontSize: 10,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
