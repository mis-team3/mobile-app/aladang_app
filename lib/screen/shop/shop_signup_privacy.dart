import 'package:aladang_app/model/privacy/Privacy.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../component/button_widget.dart';
import '../../helpdata/privacy_data.dart';
import '../../utils/constant.dart';
import 'shop_signup_onboarding_screen.dart';

class ShopSignUpPrivacy extends StatefulWidget {
  const ShopSignUpPrivacy({super.key});

  @override
  State<ShopSignUpPrivacy> createState() => _ShopSignUpPrivacyState();
}

class _ShopSignUpPrivacyState extends State<ShopSignUpPrivacy> {
  List<Privacy> privacy = [];
  bool _isLoading = false;

  void getPrivacy() async {
    _isLoading = true;

    var result = await PrivacyData().getPrivacyAll();
    setState(() {
      privacy = result.data!;
      _isLoading = false;
    });
  }

  bool? _checkBox = false;

  @override
  void initState() {
    getPrivacy();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title: Text("privacy".tr()),
        backgroundColor: primary,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: _isLoading == true
                  ? const Center(child: CircularProgressIndicator())
                  : Container(
                      width: double.infinity,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.white,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "privacy".tr() == "Privacy"
                                  ? '${privacy[0].descriptionenglish}'
                                  : '${privacy[0].description}',
                              style: const TextStyle(
                                fontSize: 14,
                                color: Colors.black,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
            ),
          ),
          Row(
            children: [
              Checkbox(
                value: _checkBox,
                checkColor: Colors.white,
                activeColor: Colors.deepPurple,
                // tristate: true,
                onChanged: (val) {
                  setState(() {
                    _checkBox = !_checkBox!;
                    _checkBox = val;
                  });
                },
              ),
              Text("accept".tr())
            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: ButtonWidget(
              name: 'next'.tr(),
              onClick: _checkBox != true
                  ? null
                  : () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                              const ShopSignUpOnboardingScreen(),
                        ),
                      );
                    },
              color: _checkBox == true ? primary : Colors.grey,
            ),
          ),
        ],
      ),
    );
  }
}
