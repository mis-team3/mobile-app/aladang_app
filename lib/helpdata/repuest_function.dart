import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';

import '../model/auth/SignInRes.dart';
import '../utils/constant.dart';

Future requestFunction({
  required String url,
  String? param1,
  String? param2,
  String? param3,
  String? param4,
  String? param5,
  String? param6,
  String? param7,
  String? param8,
  String? param9,
  String? param10,
  String? param11,
  String? param12,
  String? param13,
  String? val1,
  String? val2,
  String? val3,
  String? val4,
  String? val5,
  String? val6,
  String? val7,
  String? val8,
  String? val9,
  String? val10,
  String? val11,
  String? val12,
  String? val13,
}) async {
  var resp;

  String getToken() {
    final localStorage = LocalStorage("TOKEN_APP");
    var accessToken = localStorage.getItem("ACCESS_TOKEN");
    SignInRes loginRes = SignInRes.fromJson(accessToken);
    return loginRes.data!.token!;
  }

  if (param1 == null) {
    resp = await http.get(
      Uri.parse(url),
      headers: headerWithToken(getToken()),
    );
  }

  if (param1 != null && param2 == null) {
    resp = await http.post(
      Uri.parse(url),
      body: {
        param1: val1,
      },
      headers: headerWithToken(getToken()),
    );
  }

  if (param2 != null && param3 == null) {
    resp = await http.post(
      Uri.parse(url),
      body: {
        param1: val1,
        param2: val2,
      },
      headers: headerWithToken(getToken()),
    );
  }

  if (param3 != null && param4 == null) {
    resp = await http.post(
      Uri.parse(url),
      body: {
        param1: val1,
        param2: val2,
        param3: val3,
      },
      headers: headerWithToken(getToken()),
    );
  }

  if (param4 != null && param5 == null) {
    resp = await http.post(
      Uri.parse(url),
      body: {
        param1: val1,
        param2: val2,
        param3: val3,
        param4: val4,
      },
      headers: headerWithToken(getToken()),
    );
  }

  if (param5 != null && param6 == null) {
    resp = await http.post(
      Uri.parse(url),
      body: {
        param1: val1,
        param2: val2,
        param3: val3,
        param4: val4,
        param5: val5,
      },
      headers: headerWithToken(getToken()),
    );
  }

  if (param6 != null && param7 == null) {
    resp = await http.post(
      Uri.parse(url),
      body: {
        param1: val1,
        param2: val2,
        param3: val3,
        param4: val4,
        param5: val5,
        param6: val6,
      },
      headers: headerWithToken(getToken()),
    );
  }

  if (param7 != null && param8 == null) {
    resp = await http.post(
      Uri.parse(url),
      body: {
        param1: val1,
        param2: val2,
        param3: val3,
        param4: val4,
        param5: val5,
        param6: val6,
        param7: val7,
      },
      headers: headerWithToken(getToken()),
    );
  }

  if (param8 != null && param9 == null) {
    resp = await http.post(
      Uri.parse(url),
      body: {
        param1: val1,
        param2: val2,
        param3: val3,
        param4: val4,
        param5: val5,
        param6: val6,
        param7: val7,
        param8: val8,
      },
      headers: headerWithToken(getToken()),
    );
  }

  if (param9 != null && param10 == null) {
    resp = await http.post(
      Uri.parse(url),
      body: {
        param1: val1,
        param2: val2,
        param3: val3,
        param4: val4,
        param5: val5,
        param6: val6,
        param7: val7,
        param8: val8,
        param9: val9,
      },
      headers: headerWithToken(getToken()),
    );
  }

  if (param10 != null && param11 == null) {
    resp = await http.post(
      Uri.parse(url),
      body: {
        param1: val1,
        param2: val2,
        param3: val3,
        param4: val4,
        param5: val5,
        param6: val6,
        param7: val7,
        param8: val8,
        param9: val9,
        param10: val10,
      },
      headers: headerWithToken(getToken()),
    );
  }

  if (param11 != null && param12 == null) {
    resp = await http.post(
      Uri.parse(url),
      body: {
        param1: val1,
        param2: val2,
        param3: val3,
        param4: val4,
        param5: val5,
        param6: val6,
        param7: val7,
        param8: val8,
        param9: val9,
        param10: val10,
        param11: val11,
      },
      headers: headerWithToken(getToken()),
    );
  }

  if (param12 != null && param13 == null) {
    resp = await http.post(
      Uri.parse(url),
      body: {
        param1: val1,
        param2: val2,
        param3: val3,
        param4: val4,
        param5: val5,
        param6: val6,
        param7: val7,
        param8: val8,
        param9: val9,
        param10: val10,
        param11: val11,
        param12: val12,
      },
      headers: headerWithToken(getToken()),
    );
  }

  if (param13 != null) {
    resp = await http.post(
      Uri.parse(url),
      body: {
        param1: val1,
        param2: val2,
        param3: val3,
        param4: val4,
        param5: val5,
        param6: val6,
        param7: val7,
        param8: val8,
        param9: val9,
        param10: val10,
        param11: val11,
        param12: val12,
        param13: val13,
      },
      headers: headerWithToken(getToken()),
    );
  }
  print(getToken());
  if (resp.statusCode == 200) {
    try {
      var data = jsonDecode(resp.body);
      return data;
    } catch (e) {
      return '==============> Something went wrong, Please check again. <=============={$e}';
    }
  } else {
    return '==============> return StatusCode not 200, Please check again. <==============';
  }
}
