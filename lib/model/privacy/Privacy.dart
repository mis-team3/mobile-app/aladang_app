// ignore_for_file: file_names

class Privacy {
  int? id;
  String? description;
  String? descriptionenglish;

  Privacy({this.id, this.description});

  Privacy.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    description = json['description'];
    descriptionenglish = json['descriptionenglish'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['description'] = description;
    data['descriptionenglish'] = descriptionenglish;
    return data;
  }
}
