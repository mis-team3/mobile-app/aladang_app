import 'ReportOrderDetailRes.dart';

class ReportOrderDetail {
  int? code;
  String? message;
  List<ReportOrderDetailRes>? data;

  ReportOrderDetail({this.code, this.message, this.data});

  ReportOrderDetail.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    message = json['message'];
    if (json['data'] != null) {
      data = <ReportOrderDetailRes>[];
      json['data'].forEach((v) {
        data!.add(new ReportOrderDetailRes.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
