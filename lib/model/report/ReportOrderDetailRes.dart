class ReportOrderDetailRes {
  int? id;
  int? orderid;
  int? productid;
  String? productCode;
  String? productName;
  int? qty;
  double? price;
  double? discount;
  int? invoiceNo;
  String? date;
  int? shopId;
  String? shopName;
  int? customerId;
  String? customerName;
  String? deliveryTypeIn;
  String? currentLocation;
  String? phone;
  String? paymentType;
  String? bankName;
  String? accountNumber;
  String? accountName;
  String? receiptUpload;
  double? amountTobePaid;
  int? exchangeId;
  String? status;

  ReportOrderDetailRes(
      {this.id,
      this.orderid,
      this.productid,
      this.productCode,
      this.productName,
      this.qty,
      this.price,
      this.discount,
      this.invoiceNo,
      this.date,
      this.shopId,
      this.shopName,
      this.customerId,
      this.customerName,
      this.deliveryTypeIn,
      this.currentLocation,
      this.phone,
      this.paymentType,
      this.bankName,
      this.accountNumber,
      this.accountName,
      this.receiptUpload,
      this.amountTobePaid,
      this.exchangeId,
      this.status});

  ReportOrderDetailRes.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    orderid = json['orderid'];
    productid = json['productid'];
    productCode = json['productCode'];
    productName = json['productName'];
    qty = json['qty'];
    price = json['price'];
    discount = json['discount'];
    invoiceNo = json['invoiceNo'];
    date = json['date'];
    shopId = json['shopId'];
    shopName = json['shopName'];
    customerId = json['customerId'];
    customerName = json['customerName'];
    deliveryTypeIn = json['deliveryTypeIn'];
    currentLocation = json['currentLocation'];
    phone = json['phone'];
    paymentType = json['paymentType'];
    bankName = json['bankName'];
    accountNumber = json['accountNumber'];
    accountName = json['accountName'];
    receiptUpload = json['receiptUpload'];
    amountTobePaid = json['amountTobePaid'];
    exchangeId = json['exchangeId'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['orderid'] = this.orderid;
    data['productid'] = this.productid;
    data['productCode'] = this.productCode;
    data['productName'] = this.productName;
    data['qty'] = this.qty;
    data['price'] = this.price;
    data['discount'] = this.discount;
    data['invoiceNo'] = this.invoiceNo;
    data['date'] = this.date;
    data['shopId'] = this.shopId;
    data['shopName'] = this.shopName;
    data['customerId'] = this.customerId;
    data['customerName'] = this.customerName;
    data['deliveryTypeIn'] = this.deliveryTypeIn;
    data['currentLocation'] = this.currentLocation;
    data['phone'] = this.phone;
    data['paymentType'] = this.paymentType;
    data['bankName'] = this.bankName;
    data['accountNumber'] = this.accountNumber;
    data['accountName'] = this.accountName;
    data['receiptUpload'] = this.receiptUpload;
    data['amountTobePaid'] = this.amountTobePaid;
    data['exchangeId'] = this.exchangeId;
    data['status'] = this.status;
    return data;
  }
}
